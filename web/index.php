<?php

use YiiTest\YiiTest;

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

$yiiTest = new YiiTest(YiiTest::APP_WEB);

$application = $yiiTest->getApplication();
$application->run();
