<?php
/**
 * Created for YiiTest.
 * @author Aleksei Surkov <lexsurkov@rambler.ru>
 */

use yii\db\Query;
use yii\di\Container;
use YiiTest\Application\InvalidModelException;
use YiiTest\Application\Users\UserAdd;
use YiiTest\Application\Users\UserSearch;
use YiiTest\Domain\Users\UsersRepository;
use YiiTest\Infrastructure\Users\ValidatorSearch;
use YiiTest\Infrastructure\Users\YiiUsersRepository;
use YiiTest\Interfaces\Web\Services\Serializer;
use YiiTest\Interfaces\Web\Views\InvalidModelExceptionView;
use YiiTest\Interfaces\Web\Views\ThrowableView;

return array(
    Serializer::class => function(){
        $serializer = new Serializer();
        $serializer->addHandlers([
            Throwable::class => new ThrowableView(),
            InvalidModelException::class => new InvalidModelExceptionView(),
        ]);
        return $serializer;
    },
    UsersRepository::class => function(){
        return new YiiUsersRepository(Yii::$app->db, new Query());
    },
    UserSearch::class => function(Container $container){
        /** @var $repository UsersRepository */
        $repository = $container->get(UsersRepository::class);

        return new UserSearch($repository, new ValidatorSearch());
    },
    UserAdd::class => function(Container $container){
        /** @var $repository UsersRepository */
        $repository = $container->get(UsersRepository::class);
        $userSearch = $container->get(UserSearch::class);
        return new UserAdd($repository, new \YiiTest\Infrastructure\Users\ValidatorCreate([],$userSearch));
    }
);