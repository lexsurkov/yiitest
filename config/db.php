<?php

use yii\db\Connection;

$remoteConfig = [
    'class' => Connection::class,
    'dsn' => 'mysql:host=localhost;dbname=yiiTest',
    'username' => 'root',
    'password' => '123456',
    'charset' => 'utf8',
];

return array_merge(
    $remoteConfig,
    is_file(__DIR__.'/db.local.php') ? require __DIR__ . '/db.local.php' : []
);