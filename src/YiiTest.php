<?php
/**
 * Created for YiiTest.
 * @author Aleksei Surkov <lexsurkov@rambler.ru>
 */

declare(strict_types = 1);

namespace YiiTest;

use OutOfBoundsException;
use Yii;
use yii\console\Application as ConsoleApplication;
use yii\web\Application;

final class YiiTest
{
    const APP_WEB = 'web';
    const APP_CONSOLE = 'console';

    /**
     * YiiTest constructor.
     *
     * @param string $name Application name.
     */
    public function __construct(string $name)
    {
        Yii::setAlias('@YiiTest', __DIR__);

        switch ($name) {
            case self::APP_WEB:
                $mainConfig = require __DIR__ . '/../config/web.php';
                Yii::$container->set(Application::class, new Application($mainConfig));
                break;
            case self::APP_CONSOLE:
                $mainConfig = require __DIR__ . '/../config/console.php';
                Yii::$container->set(ConsoleApplication::class, new ConsoleApplication($mainConfig));
                break;
            default:
                throw new OutOfBoundsException('Invalid application name');
        }
    }

    public function getApplication()
    {
        return Yii::$container->get(Application::class);
    }

    public function getConsoleApplication()
    {
        return Yii::$container->get(ConsoleApplication::class);
    }

}
