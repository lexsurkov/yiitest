<?php
/**
 * Created for YiiTest.
 * @author Aleksei Surkov <lexsurkov@rambler.ru>
 */

namespace YiiTest\Domain\Users;

class User
{
    /** @var int $id */
    private $id;
    /** @var string $nick */
    private $nick;
    /** @var string $name */
    private $name;
    /** @var string $surname */
    private $surname;
    /** @var string $email */
    private $email;
    /** @var string $password */
    private $password;

    /**
     * User constructor.
     * @param int $id
     * @param string $nick
     * @param string $name
     * @param string $surname
     * @param string $email
     * @param string $password
     */
    public function __construct(int $id, string $nick, string $name, string $surname, string $email, string $password)
    {
        $this->id = $id;
        $this->nick = $nick;
        $this->name = $name;
        $this->surname = $surname;
        $this->email = $email;
        $this->password = $password;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNick(): string
    {
        return $this->nick;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

}