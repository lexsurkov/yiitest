<?php
/**
 * Created for YiiTest.
 * @author Aleksei Surkov <lexsurkov@rambler.ru>
 */

namespace YiiTest\Domain\Users;

interface UsersRepository
{
    public function getById(int $id): User;

    public function search(): array;

    public function searchByParams(array $params): array;

    public function searchByNick(string $nick): User;

    public function searchByEmail(string $email): User;

    public function store(User $entity);
}