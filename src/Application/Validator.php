<?php
/**
 * Created for YiiTest.
 * @author Aleksei Surkov <lexsurkov@rambler.ru>
 */

declare(strict_types=1);

namespace YiiTest\Application;

interface Validator
{
    public function validateData(array $data): array;
}