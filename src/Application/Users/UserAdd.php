<?php
/**
 * Created for YiiTest.
 * @author Aleksei Surkov <lexsurkov@rambler.ru>
 */

namespace YiiTest\Application\Users;

use YiiTest\Application\Validator;
use YiiTest\Domain\Users\User;
use YiiTest\Domain\Users\UsersRepository;

class UserAdd
{
    /** @var UsersRepository $repository*/
    private $repository;
    /**
     * @var Validator
     */
    private $validator;

    /**
     * UserAdd constructor.
     * @param UsersRepository $repository
     * @param Validator $validator
     */
    public function __construct(UsersRepository $repository, Validator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    public function handle(array $data)
    {
        $dto = $this->validator->validateData($data);
        $entity = new User(
            0,
            $dto['nick'],
            $dto['name'],
            $dto['surname'],
            $dto['email'],
            $dto['password']
        );

        $this->repository->store($entity);
    }
}