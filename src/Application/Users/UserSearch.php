<?php
/**
 * Created for YiiTest.
 * @author Aleksei Surkov <lexsurkov@rambler.ru>
 */

namespace YiiTest\Application\Users;

use YiiTest\Application\Validator;
use YiiTest\Domain\Users\User;
use YiiTest\Domain\Users\UsersRepository;

class UserSearch
{
    /** @var UsersRepository $repository*/
    private $repository;
    /**
     * @var Validator
     */
    private $validator;

    /**
     * UserSearch constructor.
     * @param UsersRepository $repository
     * @param Validator $validator
     */
    public function __construct(UsersRepository $repository, Validator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    /**
     * @param array $data
     * @return User[]
     */
    public function handle(array $data): array
    {
        $dto = $this->validator->validateData($data);
        return $this->repository->searchByParams($dto);
    }
}