<?php
/**
 * Created for YiiTest.
 * @author Aleksei Surkov <lexsurkov@rambler.ru>
 */

namespace YiiTest\Application;

use Exception;
use yii\base\InvalidValueException;
use yii\base\Model;
use yii\helpers\VarDumper;

class InvalidModelException extends InvalidValueException
{
    /**
     * @var Model
     */
    private $model;

    /**
     * @param Model $model
     * @param string $message
     * @param int $code
     * @param Exception $previous
     */
    public function __construct(Model $model, $message = null, $code = 0, Exception $previous = null)
    {
        $this->model = $model;
        if ($message===null && $model->hasErrors()) {
            $message = implode(' ', array_map(function ($index, $errors) {
                return implode(' ', $errors);
            }, array_keys($model->getErrors()), $model->getErrors()));
        }
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'Invalid Model';
    }

    /**
     * @return Model
     */
    public function getModel(): Model
    {
        return $this->model;
    }

    /**
     * @inheritdoc
     */
    public function __toString()
    {
        return parent::__toString() . PHP_EOL . VarDumper::dumpAsString($this->getModelDebugData());
    }
}