<?php
/**
 * Created for YiiTest.
 * @author Aleksei Surkov <lexsurkov@rambler.ru>
 */

declare(strict_types=1);

namespace YiiTest\Infrastructure\Users;

use yii\db\Connection;
use yii\db\Query;
use yii\web\NotFoundHttpException;
use YiiTest\Domain\Users\User;
use YiiTest\Domain\Users\UsersRepository;

class YiiUsersRepository implements UsersRepository
{
    /** @var Connection $connection */
    private $connection;
    /** @var Query $query */
    private $query;
    /** @var string $tableName */
    private $tableName = 'users';

    public function __construct(Connection $connection, Query $query)
    {
        $this->connection = $connection;
        $this->query = $query;
    }

    public function store(User $entity)
    {
        $count = (int) $this->getQuery()
            ->select('COUNT(*)')
            ->from($this->tableName)
            ->andWhere(['id' => $entity->getId()])
            ->scalar();
        if ($count === 0) {
            $this->insert($entity);
        } else {
            $this->update($entity);
        }
    }

    private function insert(User $entity)
    {
        $affected = $this->connection
            ->createCommand()
            ->insert($this->tableName, $this->buildModel($entity, true))
            ->execute();
        if ($affected === 0) {
            throw new \RuntimeException('UsersRepositories not insert');
        }
    }

    private function update(User $entity)
    {
        $this->connection
            ->createCommand()
            ->update($this->tableName, $this->buildModel($entity),
                'id = :id', [':id' => $entity->getId()]
            )
            ->execute();
    }

    private function buildModel(User $entity, bool $setPassword = false):array
    {
        $params = [
            'id' => $entity->getId(),
            'nick' => $entity->getNick(),
            'name' => $entity->getName(),
            'surname' => $entity->getSurname(),
            'email' => $entity->getEmail(),
        ];
        if ($setPassword) {
            $params['password'] = \Yii::$app->security->generatePasswordHash($entity->getPassword());
        }
        return $params;
    }

    public function searchByParams(array $params): array
    {
        $where = [];
        if (isset($params['id'])) {
            $where['id'] = $params['id'];
        }
        if (isset($params['nick'])) {
            $where['nick'] = $params['nick'];
        }
        if (isset($params['name'])) {
            $where['name'] = $params['name'];
        }
        if (isset($params['surname'])) {
            $where['surname'] = $params['surname'];
        }
        if (isset($params['email'])) {
            $where['email'] = $params['email'];
        }
        //var_dump($params);die();
        $models = $this->getQuery()
            ->select('*')
            ->from($this->tableName)
            ->andWhere($where)
            ->orWhere('1=0')
            ->all();

        if (!$models) {
            return [];
        }

        return array_map(function(array $model){
            return $this->buildEntity($model);
        }, $models);
    }

    public function search(): array
    {
        $models = $this->getQuery()
            ->select('*')
            ->from($this->tableName)
            ->all();

        return array_map(function(array $model){
            return $this->buildEntity($model);
        }, $models);
    }

    public function searchByNick(string $nick): User
    {
        $model = $this->getQuery()
            ->select('*')
            ->from($this->tableName)
            ->andWhere(['nick'=>$nick])
            ->one();

        if (!$model) {
            throw new NotFoundHttpException('not found by nick');
        }

        return $this->buildEntity($model);
    }

    public function searchByEmail(string $email): User
    {
        $model = $this->getQuery()
            ->select('*')
            ->from($this->tableName)
            ->andWhere(['email'=>$email])
            ->one();

        if (!$model) {
            throw new NotFoundHttpException('not found by email');
        }

        return $this->buildEntity($model);
    }

    public function getById(int $id): User
    {
        $model = $this->getQuery()
            ->select('*')
            ->from($this->tableName)
            ->andWhere(['id' => $id])
            ->one();

        if (!$model) {
            throw new NotFoundHttpException('not found by id');
        }

        return $this->buildEntity($model);
    }

    private function buildEntity(array $row): User
    {
        return new User(
            (int) $row['id'],
            $row['nick'],
            $row['name'],
            $row['surname'],
            $row['email'],
            $row['password']
        );
    }

    private function getQuery():Query
    {
        return clone $this->query;
    }
}