<?php
/**
 * Created for YiiTest.
 * @author Aleksei Surkov <lexsurkov@rambler.ru>
 */

declare(strict_types=1);

namespace YiiTest\Infrastructure\Users;

use yii\base\Model;
use yii\web\BadRequestHttpException;
use YiiTest\Application\InvalidModelException;
use YiiTest\Application\Validator;

class ValidatorSearch extends Model implements Validator
{
    public $nick;
    public $name;
    public $surname;
    public $email;

    public function rules(): array
    {
        return [
            [['nick', 'name', 'surname','email'],'string'],
            ['nick', 'match', 'pattern' => '/^[a-z]([a-z0-9]?)+$/i'],
            ['email', 'email'],
        ];
    }

    /**
     * @param array $data
     * @return array
     * @throws BadRequestHttpException
     */
    public function validateData(array $data): array
    {
        if (!$this->load($data,'')) {
            throw new BadRequestHttpException(\Yii::t('app', 'No data'));
        }
        if (!$this->validate()) {
            throw new InvalidModelException($this);
        }
        return $this->getAttributes();
    }

}
