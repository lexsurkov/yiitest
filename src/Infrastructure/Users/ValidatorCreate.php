<?php
/**
 * Created for YiiTest.
 * @author Aleksei Surkov <lexsurkov@rambler.ru>
 */

declare(strict_types=1);

namespace YiiTest\Infrastructure\Users;

use yii\base\Model;
use yii\web\BadRequestHttpException;
use YiiTest\Application\InvalidModelException;
use YiiTest\Application\Users\UserSearch;
use YiiTest\Application\Validator;

class ValidatorCreate extends Model implements Validator
{
    public $nick;
    public $name;
    public $surname;
    public $email;
    public $password;
    /**
     * @var UserSearch
     */
    private $search;

    /**
     * ValidatorCreate constructor.
     * @param array $config
     * @param UserSearch $search
     */
    public function __construct(array $config = [], UserSearch $search)
    {
        parent::__construct($config);
        $this->search = $search;
    }


    public function rules(): array
    {
        return [
            [['nick', 'name', 'surname','password','email'],'string'],
            ['nick', 'match', 'pattern' => '/^[a-z]([a-z0-9]?)+$/i'],
            ['name', 'match', 'pattern' => '/^[А-я]+$/u'],
            ['surname', 'match', 'pattern' => '/^[А-я]+$/u'],
            ['password', 'string', 'min' => 5],
            ['email', 'email'],
            ['nick', 'validateNick'],
            ['email', 'validateEmail'],
        ];
    }

    public function validateNick($attribute)
    {
        try {
            $users = $this->search->handle([$attribute => $this->$attribute]);
            if ($users) {
                $this->addError($attribute, 'Пользователь с таким никнеймом зарегистрирован');
            }
        } catch (\Exception $exception) {
            $this->addError($attribute, $exception->getMessage());
        }
    }

    public function validateEmail($attribute)
    {
        try {
            $users = $this->search->handle([$attribute => $this->$attribute]);
            if ($users) {
                $this->addError($attribute, 'На эту электронную почту уже зарегистрирован аккаунт');
            }
        } catch (\Exception $exception) {
            $this->addError($attribute, $exception->getMessage());
        }
    }

    /**
     * @param array $data
     * @return array
     * @throws BadRequestHttpException
     */
    public function validateData(array $data): array
    {
        if (!$this->load($data,'')) {
            throw new BadRequestHttpException(\Yii::t('app', 'No data'));
        }
        if (!$this->validate()) {
            throw new InvalidModelException($this);
        }
        return $this->getAttributes();
    }

}
