<?php
/**
 * Created for YiiTest.
 * @author Aleksei Surkov <lexsurkov@rambler.ru>
 */

declare(strict_types=1);

namespace YiiTest\Interfaces\Web\Controllers;

use Yii;
use yii\base\Module;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use YiiTest\Application\Users\UserAdd;
use YiiTest\Application\Users\UserSearch;
use YiiTest\Interfaces\Web\Services\Serializer;

class SiteController extends Controller
{
    /** @var UserAdd $add */
    private $add;
    /** @var UserSearch $search */
    private $search;

    /**
     * SiteController constructor.
     * @param string $id
     * @param Module $module
     * @param array $config
     * @param Serializer $serializer
     * @param UserAdd $add
     * @param UserSearch $search
     */
    public function __construct(
        string $id, Module $module, array $config = [],
        Serializer $serializer,
        UserAdd $add,
        UserSearch $search
    )
    {
        parent::__construct($id, $module, $config, $serializer);
        $this->add = $add;
        $this->search = $search;
    }


    public function runAction($id, $params = [])
    {
        if (\in_array($id, ['check-user', 'register']))
            Yii::$app->response->format = Response::FORMAT_JSON;
        return parent::runAction($id, $params);
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        //$users = $this->search->handle();

        return $this->render('index');
    }

    /**
     * @throws BadRequestHttpException
     */
    public function actionCheckUser()
    {
        try {
            $users = $this->search->handle(Yii::$app->request->post());
            if ($users && Yii::$app->request->post('nick')) {
                throw new BadRequestHttpException('Пользователь с таким никнеймом зарегистрирован');
            }

            if ($users && Yii::$app->request->post('email')) {
                throw new BadRequestHttpException('На эту электронную почту уже зарегистрирован аккаунт');
            }
        } catch (\Exception $exception) {
            throw new BadRequestHttpException($exception->getMessage());
        }
    }

    public function actionRegister()
    {
        $this->add->handle(Yii::$app->request->post());
    }

}
