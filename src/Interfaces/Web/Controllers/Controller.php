<?php
/**
 * Created for YiiTest.
 * @author Aleksei Surkov <lexsurkov@rambler.ru>
 */

declare(strict_types=1);

namespace YiiTest\Interfaces\Web\Controllers;

use Yii;
use yii\base\Module;
use yii\web\Controller as YiiWebController;
use yii\web\Response;
use YiiTest\Interfaces\Web\Services\Serializer;

abstract class Controller extends YiiWebController
{

    public $layout = '@YiiTest/Interfaces/Web/Views/layouts/main';

    /** @var Serializer */
    private $serializer;

    public function __construct(
        string $id, Module $module, array $config = [],
        Serializer $serializer
    )
    {
        parent::__construct($id, $module, $config);
        $this->serializer = $serializer;
    }

    public function runAction($id, $params = [])
    {
        if (Yii::$app->response->format === Response::FORMAT_JSON) {
            try {
                $result = parent::runAction($id, $params);
            } catch (\Throwable $exception) {
                $result = $exception;
            }
            return $this->serializer->serialize($result);
        }
        return parent::runAction($id, $params);
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function getViewPath()
    {
        return Yii::getAlias("@YiiTest/Interfaces/Web/Views/{$this->action->controller->id}");
    }

}
