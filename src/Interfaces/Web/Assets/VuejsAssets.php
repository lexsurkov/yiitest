<?php
/**
 * Created for YiiTest.
 * @author Aleksei Surkov <lexsurkov@rambler.ru>
 */

declare(strict_types=1);

namespace YiiTest\Interfaces\Web\Assets;

use yii\web\AssetBundle;
use yii\web\View;

final class VuejsAssets extends AssetBundle
{
    /**
     * {@inheritdoc}
     */
    public $sourcePath = '@bower/vue/dist';

    /**
     * {@inheritdoc}
     */
    public $js = [
        'vue.min.js',
    ];

    /**
     * {@inheritdoc}
     */
    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];

    /**
     * {@inheritdoc}
     */
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
