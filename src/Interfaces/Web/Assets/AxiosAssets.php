<?php
/**
 * Created for YiiTest.
 * @author Aleksei Surkov <lexsurkov@rambler.ru>
 */

declare(strict_types=1);

namespace YiiTest\Interfaces\Web\Assets;

use yii\web\AssetBundle;
use yii\web\View;

final class AxiosAssets extends AssetBundle
{
    /**
     * {@inheritdoc}
     */
    public $sourcePath = '@bower/axios/dist';

    /**
     * {@inheritdoc}
     */
    public $js = [
        'axios.min.js',
    ];

    /**
     * {@inheritdoc}
     */
    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];
}