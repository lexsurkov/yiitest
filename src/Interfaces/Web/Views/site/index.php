<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

use yii\helpers\Html;
use YiiTest\Interfaces\Web\Assets\AxiosAssets;
use YiiTest\Interfaces\Web\Assets\VuejsAssets;

VuejsAssets::register($this);
AxiosAssets::register($this);

$this->title = 'Регистрация';
?>
<div id='site-login' class="site-login">
    <div class="alert alert-success" v-if="regOk">
        Регистрация завершена
    </div>

    <div v-if="!regOk">
        <h1><?= Html::encode($this->title) ?></h1>

        <form class="form-horizontal" :submit.prevent="onSubmit">
            <div class="row">
                <div class="col-sm-12">
                    <div :class="['form-group', errors['nick'] ? 'has-error' : '']">
                        <label class="control-label col-sm-2">
                            Никнейм
                        </label>
                        <label id="label-nick" class="control-label col-sm-1">
                            <span class=""></span>
                        </label>
                        <div class="col-sm-4">
                            <input  type="text" class="form-control" id="nick" placeholder="Никнейм" v-model="nick">
                        </div>
                        <div class="col-sm-5">
                            <span class="help-block" v-if="errors['nick']">{{ errors['nick'][0] }}</span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div :class="['form-group', errors['name'] ? 'has-error' : '']">
                        <label class="control-label col-sm-2">
                            Имя
                        </label>
                        <label id="label-name" class="control-label col-sm-1">
                            <span class=""></span>
                        </label>
                        <div class="col-sm-4">
                            <input @input="changeName" @change="changeName" type="text" class="form-control" id="name" placeholder="Имя" v-model="name">
                        </div>
                        <div class="col-sm-5">
                            <span class="help-block" v-if="errors['name']">{{ errors['name'][0] }}</span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div :class="['form-group', errors['surname'] ? 'has-error' : '']">
                        <label class="control-label col-sm-2">
                            Фамилия
                        </label>
                        <label id="label-surname" class="control-label col-sm-1">
                            <span class=""></span>
                        </label>
                        <div class="col-sm-4">
                            <input @input="changeSurname" @change="changeSurname" type="text" class="form-control" id="surname" placeholder="Фамилия" v-model="surname">
                        </div>
                        <div class="col-sm-5">
                            <span class="help-block" v-if="errors['surname']">{{ errors['surname'][0] }}</span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div :class="['form-group', errors['email'] ? 'has-error' : '']">
                        <label class="control-label col-sm-2">
                            Электронная почта
                        </label>
                        <label id="label-email" class="control-label col-sm-1">
                            <span class=""></span>
                        </label>
                        <div class="col-sm-4">
                            <input @input="changeEmail" @change="changeEmail" type="email" class="form-control" id="email" placeholder="Электронная почта" v-model="email">
                        </div>
                        <div class="col-sm-5">
                            <span class="help-block" v-if="errors['email']">{{ errors['email'][0] }}</span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div :class="['form-group', errors['password'] ? 'has-error' : '']">
                        <label class="control-label col-sm-2">
                            Пароль
                        </label>
                        <label id="label-password" class="control-label col-sm-1">
                            <span class=""></span>
                        </label>
                        <div class="col-sm-4">
                            <input @input="changePassword" @change="changePassword" type="password" class="form-control" id="password" placeholder="Пароль" v-model="password">
                        </div>
                        <div class="col-sm-5">
                            <span class="help-block" v-if="errors['password']">{{ errors['password'][0] }}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-offset-1 col-lg-11">
                    <button type="submit" class="btn btn-success" :disabled="!send">
                        Готово
                    </button>
                </div>
            </div>
        </form>
    </div>

</div>

<script type="text/javascript">
    new Vue({
        el: '#site-login',
        data: {
            nick: null,
            name: null,
            surname: null,
            email: null,
            password: null,
            // Массив ошибок
            errors: {},
            // Переменные отправки запроса
            send: false,
            regOk:false
        },
        methods: {
            validEmail: function (email) {
                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            },
            changeNick: function(event){
                var self = this;
                var element = event.target;
                Vue.delete(self.errors, 'nick');
                if (!(/^[a-z]([a-z0-9]?)+$/i).test(element.value)){
                    Vue.set(self.errors, 'nick', {0:'Никнейм должен содержать цифры и буквы латинского алфавита и начинаться с буквы'});
                } else {
                    var csrf = '<?= Yii::$app->request->csrfToken ?>';
                    axios.post('/site/check-user', {
                        nick: self.nick
                    }, {
                        headers: {
                            'Content-Type': 'application/json;charset=UTF-8',
                            'X-CSRF-Token': csrf
                        }
                    }).then(function (response) {
                        //
                    }).catch(function (error) {
                        Vue.set(self.errors, 'nick', {0:error.response.data.message});
                        $('#label-nick span').attr('class', self.errors['nick'] ? 'fa fa-exclamation' : 'fa fa-check');
                    });
                }
                $('#label-nick span').attr('class', self.errors['nick'] ? 'fa fa-exclamation' : 'fa fa-check');
                event.preventDefault();
            },
            changeName:function (event) {
                var self = this;
                var element = event.target;
                Vue.delete(self.errors, 'name');
                if (!(/^[А-Я]+$/i).test(element.value)) {
                    Vue.set(self.errors, 'name', {0:'Только русские буквы'});
                }
                $('#label-name span').attr('class', self.errors['name'] ? 'fa fa-exclamation' : 'fa fa-check');
                event.preventDefault();
            },
            changeSurname:function (event) {
                var self = this;
                var element = event.target;
                Vue.delete(self.errors, 'surname');
                if (!(/^[А-Я]+$/i).test(element.value)) {
                    Vue.set(self.errors, 'surname', {0:'Только русские буквы'});
                }
                $('#label-surname span').attr('class', self.errors['surname'] ? 'fa fa-exclamation' : 'fa fa-check');
                event.preventDefault();
            },
            changeEmail: function (event) {
                var self = this;
                //var element = event.target;
                Vue.delete(self.errors, 'email');
                if (!this.validEmail(this.email)) {
                    Vue.set(self.errors, 'email', {0:'Не верный адрес электронной почты'});
                } else {
                    var csrf = '<?= Yii::$app->request->csrfToken ?>';
                    axios.post('/site/check-user', {
                        email: self.email
                    }, {
                        headers: {
                            'Content-Type': 'application/json;charset=UTF-8',
                            'X-CSRF-Token': csrf
                        }
                    }).then(function (response) {
                        //
                    }).catch(function (error) {
                        Vue.set(self.errors, 'email', {0:error.response.data.message});
                        $('#label-email span').attr('class', self.errors['email'] ? 'fa fa-exclamation' : 'fa fa-check');
                    });
                }
                $('#label-email span').attr('class', self.errors['email'] ? 'fa fa-exclamation' : 'fa fa-check');
                event.preventDefault();
            },
            changePassword: function (event) {
                var self = this;
                var element = event.target;
                Vue.delete(self.errors, 'password');
                if (element.value.length < 5) {
                    Vue.set(self.errors, 'password', {0:'Пожалуйста, выдумайте пароль длиннее 5 символов'});
                }
                $('#label-password span').attr('class', self.errors['password'] ? 'fa fa-exclamation' : 'fa fa-check');
                event.preventDefault();
            },
            onSubmit: function (event) {
                var self = this;
                var csrf = '<?= Yii::$app->request->csrfToken ?>';
                axios.post('/site/register', {
                    nick: self.nick,
                    name: self.name,
                    surname: self.surname,
                    email: self.email,
                    password: self.password
                }, {
                    headers: {
                        'Content-Type':'application/json;charset=UTF-8',
                        'X-CSRF-Token':csrf
                    }
                }).then(function (response) {
                    self.regOk = true;
                }).catch(function (error) {
                    if (error.response) {
                        var data = error.response.data;
                        if (data.details !== undefined) {
                            Object.keys(data.details).forEach(function (attribute) {
                                Vue.set(self.errors, attribute, data.details[attribute]);
                            });
                        }
                        window.scrollTo(0, 0);
                    } else {
                        alert('Something is wrong on server side: ' + error.message);
                    }
                });
            }
        },
        mounted: function () {
        },
        updated: function() {
            this.send = this.nick && this.name && this.surname && this.email && this.password && Object.keys(this.errors).length === 0;
        }
    });
</script>