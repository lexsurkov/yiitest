<?php
/**
 * Created for YiiTest.
 * @author Aleksei Surkov <lexsurkov@rambler.ru>
 */

declare(strict_types = 1);

namespace YiiTest\Interfaces\Web\Views;

use YiiTest\Application\InvalidModelException;
use Throwable;
use yii\web\HttpException;

final class InvalidModelExceptionView
{
    /**
     * @param InvalidModelException $exception
     *
     * @return array
     */
    public function __invoke(InvalidModelException $exception): array
    {
        //var_dump($exception);die();
        $statusCode = $exception->getCode() > 0 ? $exception->getCode() : 500;

        $statusCode = ($exception->getCode() < 100 || $exception->getCode() >= 600) ? 500 : $statusCode;
        if ($exception instanceof HttpException) {
            $statusCode = $exception->statusCode;
            //$statusCode = ($statusCode < 100 || $statusCode >= 600) ? 500 : $statusCode;
        }
        \Yii::$app->response->setStatusCode($statusCode);
        return [
            'status' => $statusCode,
            'message' => $exception->getMessage(),
            'details' => $exception->getModel()->getErrors()
        ];
    }
}
