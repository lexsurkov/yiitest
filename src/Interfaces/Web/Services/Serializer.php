<?php
/**
 * Created for YiiTest.
 * @author Aleksei Surkov <lexsurkov@rambler.ru>
 */

declare(strict_types = 1);

namespace YiiTest\Interfaces\Web\Services;

use Throwable;
use yii\rest\Serializer as RestSerializer;

final class Serializer extends RestSerializer
{
    /**
     * @var array
     */
    private $handlers = [];

    /**
     * @param array $handlers
     */
    public function addHandlers(array $handlers)
    {
        foreach ($handlers as $class => $handler) {
            $this->handlers[$class] = $handler;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function serialize($data)
    {

        if (\is_object($data) && isset($this->handlers[\get_class($data)])) {
            $handler = $this->handlers[\get_class($data)];

            return $handler($data);
        }
        if ($data instanceof Throwable) {
            $handler = $this->handlers[Throwable::class];

            if (isset($this->handlers[\get_class($data)])) {
                $handler = $this->handlers[\get_class($data)];
            } elseif (isset($this->handlers[Throwable::class])) {
                $handler = $this->handlers[Throwable::class];
            }

            if ($handler !== null) {
                return $handler($data);
            }
        }

        if (\is_array($data)) {
            $array = [];
            foreach ($data as $index => $item){
                if (\is_object($item) || \is_array($item)){
                    //var_dump($item);die();
                    $array[$index] = $this->serialize($item);
                } else {
                    $array[$index] = $item;
                }
            }
            return $array;
        }
        return parent::serialize($data);
    }
}
