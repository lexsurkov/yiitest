<?php

use YiiTest\Interfaces\Console\Migrations\BaseMigration;

class m180905_235250_ct_users extends BaseMigration
{
    private $tableName = '{{%users}}';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(11)->notNull(),
            'nick' => $this->string(),
            'name' => $this->integer(),
            'surname' => $this->string(),
            'email' => $this->string(),
            'password' => $this->string(),
        ], $this->tableOptions);

        $this->createIndex('iNick', $this->tableName, 'nick', true);
        $this->createIndex('iName', $this->tableName, 'name');
        $this->createIndex('iSurname', $this->tableName, 'surname');
        $this->createIndex('iEmail', $this->tableName, 'email');
    }

    public function down(): bool
    {
        $this->dropTable($this->tableName);
        return true;
    }
}
