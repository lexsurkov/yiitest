<?php

namespace YiiTest\Interfaces\Console\Migrations;

use YiiTest\Interfaces\Console\Migrations\traits\TextTypesTrait;
use yii\db\Migration;

/**
 * Общий класс для миграций
 *
 * Необходим для инициализации общих параметров.
 *
 * @version 1.0
 * @package app/migrations
 */

abstract class BaseMigration extends Migration
{
    use TextTypesTrait;
    /**
     * Дополнительные опции при создании таблиц в БД
     * @var string
     */
    protected $tableOptions;
    /**
     * RESTRICT параметр для создания индексов
     * @var string
     */
    protected $restrict = 'RESTRICT';
    /**
     * CASCADE параметр для создания индексов
     * @var string
     */
    protected $cascade = 'CASCADE';
    /**
     * Тип базы данных
     * @var string
     */
    protected $dbType;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        switch ($this->db->driverName) {
            case 'mysql':
                $this->tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=MyISAM';
                $this->dbType = 'mysql';
                break;
            case 'pgsql':
                $this->tableOptions = null;
                $this->dbType = 'pgsql';
                break;
            case 'dblib':
            case 'mssql':
            case 'sqlsrv':
                $this->restrict = 'NO ACTION';
                $this->tableOptions = null;
                $this->dbType = 'sqlsrv';
                break;
            default:
                throw new \RuntimeException('Your database is not supported!');
        }
    }

    /**
     * Этот метод содержит логику, выполняемую при применении миграции.
     */
    public function up()
    {
        try {
            $this->safeUp();
        } catch (\Exception $e) {
            return $this->migrateContinue('Exception: ' . $e->getMessage() . ' (' . $e->getFile() . ':' . $e->getLine() . ")\n");
        }
        return null;
    }

    protected function migrateContinue($textError = null): bool
    {
        echo PHP_EOL . $textError;
        $answer = readline('Произошли ошибки выполнения, желаете продолжить? (yes|no) [no]: ');
        while ($answer === null && !\in_array($answer, ['yes', 'no'])){
            $answer = readline('Произошли ошибки выполнения, желаете продолжить? (yes|no) [no]: ');
        }
        return $answer === 'yes';
    }
}
