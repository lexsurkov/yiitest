<?php
/**
 * Created for YiiTest.
 * @author Aleksei Surkov <lexsurkov@rambler.ru>
 */

declare(strict_types=1);

namespace YiiTest\Interfaces\Console\Controllers;

use yii\console\Controller;

class TestController extends Controller
{
    public function actionTest()
    {
        echo 'test' . "\n";
    }
}